## Mode d'emploi

 * Copier votre cle publique dans ./cle_publique
 * commande : vagrant up
 * premier déploiement : ansible-playbook -i inventory play.yml

Suivant le numéro de TP affecté (voir mon mail ) :

### TP 0 :

 * Sur le serveur theApache : Ajouter un moteur de base de données Mariadb/Mysql + une base de donnée avec des utilisateurs/mot de passe, définie dans une liste sous format yaml
 * Sur le même serveur : Ajouter un script de sauvegarde de mysql (basique) et configurer une crontab pour effectuer la sauvegarde tous les jours de la base
 * Ajouter un role commun pour les deux groupes de machine pour installer une liste de package prédéfinie (exemple : nmon, htop, tmux, screen)

### TP 1 :

 * Ajouter un certificat SSL autosigné pour la connexion nginx / client web
 * Ajouter php à apache et insérer une page de test phpinfo() par défaut sur les sites
 * Ajouter des regles iptables sur le nginx et apache pour n'accepter que les flux http et ssh nécessaire au bon fonctionnement

## Quelques rappels

 * Pour tester le fonctionnement du loadbalancer : http://192.168.33.18:9000 (et plein de F5 pour basculer d'un apache à l'autre )

 * Je ferais avec vos dépots la même chose que le mode d'emploi => Je vous conseille donc de rejouer pour être sur que ça fonctionnera.

 * Ne pas utiliser les modules "command" ou "shell" , sauf extreme urgence.

 * Vous pouvez rendre votre travail via framagit ou via mail

 * n'hésitez pas à rajouter un petit README si vous voyez la necessité #JaimeLaDoc

 * j'espère que vous avez lu les rappels.
